/**
 * Created by KW on 10/3/2017.
 */
public class Account {
    private double account;

    public Account(double amount) {
        this.account = account;
    }

    public double getAccount() {
        return account;
    }

    public void setAccount(double account) {
        this.account = account;
    }

    public void addToAccount (double amount) {
        synchronized (this) {
            account += amount;
            System.out.println("Your account: " + account);
        }
    }
    public void subFromAccount (double amount) {
        synchronized (this) {
            account -= amount;
            System.out.println("Your account: " + account);
        }
    }
    public void balance (){
        System.out.println("Your account: "+ account);
    }
}
