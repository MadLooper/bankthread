/**
 * Created by KW on 10/3/2017.
 */
public class BankTransaction implements Runnable{
    private Type type;
    private double amount;
    private Account account;


    public BankTransaction(Type type, double amount, Account account) {
        this.type = type;
        this.amount = amount;
        this.account = account;
    }

    public void run() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            switch (type){
                case ADD:
                    Thread.sleep(1000);
                    account.addToAccount(amount);
                    break;

                case SUB:
                    Thread.sleep(1000);
                    account.subFromAccount(amount);

                case BALANCE:
                    Thread.sleep(1000);
                    account.balance();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
