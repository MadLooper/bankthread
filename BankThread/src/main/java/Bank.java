import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by KW on 10/3/2017.
 */
public class Bank {
    private Account account= new Account(10000);
    private ExecutorService service = Executors.newFixedThreadPool(15);
    public void addToAccount (double amount){
service.submit(new BankTransaction(Type.ADD,amount,account));
    }

    public void subFromAmount (double amount){
        service.submit(new BankTransaction(Type.SUB,amount,account));
    }
    public void balance (){
        service.submit(new BankTransaction(Type.BALANCE,0.0,account));
    }

}
