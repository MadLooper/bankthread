import java.util.Scanner;

/**
 * Created by KW on 10/3/2017.
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("Witaj w naszym banku. Co chcesz dzisiaj u nas zrobic?");
        Scanner scanner = new Scanner(System.in);
        String word = scanner.nextLine();
        String[] split = word.split(" ");
        Bank bank = new Bank();

        for (int i = 0; i < 10000; i++) {
            if (i % 2 == 0) {
                bank.subFromAmount(5);
            } else {
                bank.addToAccount(5);
            }
        }



        while (scanner.hasNextLine()) {
            if (split[0].toLowerCase().equals("add")) {
                bank.addToAccount(Double.parseDouble(split[1]));

            } else if (split[0].toLowerCase().equals("sub")) {
                bank.subFromAmount(Double.parseDouble(split[1]));

            } else if (split[0].toLowerCase().equals("balance")) {
                bank.balance();

            }
        }
    }
}
